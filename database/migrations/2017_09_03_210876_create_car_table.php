<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car', function (Blueprint $table) {
            $table->increments('id');
            $table->string('owner_name');
            $table->string('description');
            $table->decimal('price', 10, 2);
            $table->integer('year_of_issue');
            $table->integer('category');
            $table->integer('car_brand_id')->unsigned()->index();
        });

        Schema::table('car', function (Blueprint $table) {
            $table->foreign('car_brand_id')->references('id')->on('car_brand');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car');
    }
}
