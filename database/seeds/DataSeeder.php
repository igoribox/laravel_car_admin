<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_brand')->insert([
            'name' => 'BMW',
        ]);

        DB::table('car_brand')->insert([
            'name' => 'Audi',
        ]);

        DB::table('car_brand')->insert([
            'name' => 'Honda',
        ]);
    }
}
