// request permission for notifications on page load
document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
        alert('Desktop notifications not available in your browser.');
        return;
    }
    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

$(document).ready(function () {
    initPusher();
});


function initPusher() {
    var pusher_config = $('#pusher_conf');
    var pusher = new Pusher(
        pusher_config.attr('attr_pusher_key'),
        {
            cluster: pusher_config.attr('attr_pusher_cluster'),
            encrypted: true
        }
    );
    var channel = pusher.subscribe('my-channel');
    channel.bind('my-event', function (data) {
        notifyMe(data.message)
    });
}

// create notify for user
function notifyMe(message) {
    //if user deny notification then show him alert in browser
    if (Notification.permission !== "granted") {
        $('.alert-content').text(message);
        $('.alert-success').show('fast');
    } else {
        new Notification('Новое сообщение', {
            icon: 'http://www.iconsdb.com/icons/preview/caribbean-blue/message-2-xxl.png',
            body: message,
        });
    }
}

//vue js
var app = new Vue({
    el: '#app',
    data: {
        carPrice: 0,
        ownerName: '',
        yearOfIssue: 1800,
        carBrandId: 1,
        description: '',
        showPreview: false, // check that user fill description and ownerName
    },
    delimiters: ['<%', '%>'], // change delimiters fro laravel blade
    methods: {
        formatPrice: function (event) {
            this.carPrice = parseFloat(this.carPrice).toFixed(2);
        },
        closeAlertDiv: function (event) {
            $('.alert-success').hide('fast');
        }
    },
    computed: {
        advertisePreview: function () {
            return 'Превью - Марка: ' + this.carBrandName + ' ' + this.yearOfIssue + ' год ' + this.description
        },
        carBrandName: function () {
            return $("#car_brand_id option:selected").text();
        },
        // watch description and ownerName for changes
        descriptionPlusOwnerName: function () {
            return {
                description: this.description,
                ownerName: this.ownerName
            }
        }
    },
    watch: {
        descriptionPlusOwnerName: function (newQuestion) {
            if (this.descriptionPlusOwnerName['description'] && this.descriptionPlusOwnerName['ownerName']) {
                this.showPreview = true
            } else {
                this.showPreview = false
            }
        }
    }
})
