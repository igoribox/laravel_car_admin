<?php

namespace App\Providers;

use App\Helpers\MyPusher;
use Illuminate\Support\ServiceProvider;

class PusherServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('pusher', function ($app) {
            return (new MyPusher())->getPusher();
        });
    }
}
