<?php

namespace App\Helpers;


use Pusher\Pusher;

class MyPusher{

    private $pusher;

    public function __construct(){

        $options = array(
            'cluster' => env('PUSHER_CLUSTER'),
            'encrypted' => true
        );

        $this->pusher = new Pusher(
            env('PUSHER_KEY'),
            env('PUSHER_SECRET'),
            env('PUSHER_APPID'),
            $options
        );

    }

    public function getPusher(){
        return $this->pusher;
    }

}