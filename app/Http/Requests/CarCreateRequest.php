<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class CarCreateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'owner_name' => 'string|required|min:3|max:100',
            'description' => 'string|required|min:3|max:100',
            'year_of_issue' => 'required|date_format:Y',
            'price' => "required|regex:/^\d*(\.\d{1,2})?$/|min:1|max:99999999.99",
            'car_brand_id' => 'required|exists:car_brand,id',
        ];
    }
}