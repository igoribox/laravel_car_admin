<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarBrand extends Model
{
    public $timestamps = false;

    protected $table = 'car_brand';

}
